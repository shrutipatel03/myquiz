package com.example.myquiz.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myquiz.R;
import com.example.myquiz.model.McqAnswerModel;
import com.example.myquiz.model.QuestionContainerModel;
import com.example.myquiz.model.QuestionDataModel;
import com.example.myquiz.model.QuestionModel;
import com.example.myquiz.myUtils.APIinterface;
import com.example.myquiz.myUtils.ApiClient;
import com.example.myquiz.myUtils.GeneralUtils;
import com.example.myquiz.myUtils.LoadingView;

import net.crosp.libs.android.circletimeview.CircleTimeView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class QuizActivity extends AppCompatActivity {

    @BindView(R.id.tvQestionNo)
    TextView tvQuestionNo;
    @BindView(R.id.tvRightAns)
    TextView tvRightAns;
    @BindView(R.id.tvQuestion)
    TextView tvQuestion;
    QuestionDataModel questionDataModel;
    @BindView(R.id.llContainer)
    LinearLayout llContainer;
    @BindView(R.id.tvAttemptedQuestion)
    TextView tvAttemptedQuestion;
    @BindView(R.id.circle_timer_view)
    CircleTimeView circle_timer_view;
    List<QuestionDataModel> questionDataModelList;
    List<View> viewList;
    LoadingView loadingView;
    String TAG = "QuizActivity";
    int questionCounter = 0;
    int rightAnsCounter =0;
    boolean isUser_click = false;
    boolean isSystem_click = false;
    QuestionModel questionModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        ButterKnife.bind(this);

        initAllControls();
    }

    private void initAllControls() {
        loadingView = new LoadingView(this);
        getSupportActionBar().setTitle("Quiz Activity");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        questionDataModelList = new ArrayList<>();
        questionModel = new QuestionModel();
        viewList = new ArrayList<>();
        getDatafromServer();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getDatafromServer(){
        loadingView.showDialog();
        APIinterface apIinterface = ApiClient.getClient().create(APIinterface.class);
        Call<QuestionModel> questionModelCall = apIinterface.getQuestion(GeneralUtils.getAuthToken(),"1","1","starter");
        questionModelCall.enqueue(new Callback<QuestionModel>() {
            @Override
            public void onResponse(Call<QuestionModel> call, Response<QuestionModel> response) {
                loadingView.hideDialog();
                QuestionModel questionModel = response.body();
                if(questionModel != null){
                    if(questionModel.getStatus()){
                        questionDataModelList.addAll(questionModel.getData());

                        showQuestion(questionDataModelList.get(questionCounter));
//                        Toast.makeText(QuizActivity.this, "size:- "+questionDataModelList.size(), Toast.LENGTH_SHORT).show();

                    }
                    else {
//                        Toast.makeText(getApplicationContext(),"data found",Toast.LENGTH_LONG).show();
                    }

                }
                else {
                    Log.d(TAG,"data"+questionDataModelList);
                    Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<QuestionModel> call, Throwable t) {
                loadingView.hideDialog();
//                Toast.makeText(getApplicationContext(),"something wrong",Toast.LENGTH_LONG).show();
            }
        });
    }


    public void showQuestion(QuestionDataModel questionDataModel) {
        this.questionDataModel = questionDataModel;
        tvQuestion.setText(questionDataModel.getMqQuestion());
        isUser_click = false;
        isSystem_click = false;
        llContainer.removeAllViews();
        viewList.clear();



        Log.d(TAG, "answer data " + questionDataModel.getMcqAnswerModel());

        for (McqAnswerModel mcqAnswerModel : questionDataModel.getMcqAnswerModel()) {
            View rootView = getLayoutInflater().inflate(R.layout.answer_btn_inflater, null);
            LinearLayout llAns = rootView.findViewById(R.id.llAnsContainer);
            TextView tvAns = rootView.findViewById(R.id.tvAns);
            tvAns.setText(mcqAnswerModel.getAnswer());

//            Toast.makeText(getApplicationContext(),mcqAnswerModel.getAnswer(),Toast.LENGTH_LONG).show();


            tvAns.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (isUser_click) {
                        return;
                    }
                    isUser_click = true;
                    if (isSystem_click) {
                        mcqAnswerModel.setUser_select(false);
                        mcqAnswerModel.setSystem_select(true);
                    } else {
                        mcqAnswerModel.setUser_select(true);
                        mcqAnswerModel.setSystem_select(false);
                    }




                        circle_timer_view.stopTimer();

                         Toast.makeText(getApplicationContext(),mcqAnswerModel.getAnswer(),Toast.LENGTH_LONG).show();

                        if (mcqAnswerModel.getMcIsTrueAnswer().equals("right")) {

                            if(isSystem_click)
                            {
                                questionDataModel.setIs_user_select_right(false);

                            }else {
                                questionDataModel.setIs_user_select_right(true);
                                rightAnsCounter++;

                            }


                            Log.d(TAG, "right ans" + rightAnsCounter);

                            tvAns.setBackgroundResource(R.drawable.right_ans_background);
                            tvAns.setTextColor(Color.WHITE);
                        } else {

                            tvAns.setBackgroundResource(R.drawable.wrong_ans_background);
                            tvAns.setTextColor(Color.WHITE);
                            int counter = 0, rightPosition = 0;
                            for (McqAnswerModel mcqAnswerModel : questionDataModel.getMcqAnswerModel()) {
                                if (mcqAnswerModel.getMcIsTrueAnswer().equals("right")) {
                                    rightPosition = counter;
                                }
                                counter++;

                            }
                            View tempView = viewList.get(rightPosition);
                            TextView tvAns = tempView.findViewById(R.id.tvAns);
                            tvAns.setBackgroundResource(R.drawable.right_ans_background);
                            tvAns.setTextColor(Color.WHITE);


                        }


//
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (questionCounter < questionDataModelList.size()) {
                                Log.d(TAG, "counter " + questionCounter);
                                showQuestion(questionDataModelList.get(questionCounter));

                            } else {
                                QuestionContainerModel questionContainerModel = new QuestionContainerModel();
                                questionContainerModel.setQuestionDataModelList(questionDataModelList);
                                Intent intent = new Intent(QuizActivity.this, QuizDetailActivity.class);
                                intent.putExtra("data",questionContainerModel);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }


                        }
                    }, 2000);
                    questionCounter++;
                    tvQuestionNo.setText("" + questionCounter);
                    tvAttemptedQuestion.setText("" + questionCounter);
                    tvRightAns.setText("" + rightAnsCounter);

                }
            });


            llContainer.addView(rootView);
            viewList.add(rootView);


        }
        circle_timer_view.setCurrentTime(Integer.parseInt(questionDataModel.getMqTime()));
        circle_timer_view.startTimer();
        circle_timer_view.setCircleTimeListener(new CircleTimeView.CircleTimeListener() {
            @Override
            public void onTimeManuallySet(long time) {

            }

            @Override
            public void onTimeManuallyChanged(long time) {

            }

            @Override
            public void onTimeUpdated(long time) {
                if(time == 0) {
                    int counter = 0;
                    for (McqAnswerModel mcqAnswerModel : questionDataModel.getMcqAnswerModel()) {
                        if (mcqAnswerModel.getMcIsTrueAnswer().equals("right")) {
                            View tempView = viewList.get(counter);
                            TextView tvAns = tempView.findViewById(R.id.tvAns);
                            isSystem_click = true;
                            tvAns.performClick();

                        }
                        counter++;
                    }
                }
            }
        });
    }
}
