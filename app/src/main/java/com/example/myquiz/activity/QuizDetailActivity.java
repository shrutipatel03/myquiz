package com.example.myquiz.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.myquiz.R;
import com.example.myquiz.model.QuestionContainerModel;
import com.example.myquiz.model.QuestionDataModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuizDetailActivity extends AppCompatActivity {

    @BindView(R.id.tvTotalQuestion)
    TextView tvTotalQuestion;
    @BindView(R.id.tvTotalRightAnswer)
    TextView tvTotalRightAnswer;
    QuestionContainerModel questionContainerModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_detail);
        ButterKnife.bind(this);
        initAllControls();
    }

    private void initAllControls() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Quiz Detail Activity");
        Intent i = getIntent();
        questionContainerModel = i.getParcelableExtra("data");


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
