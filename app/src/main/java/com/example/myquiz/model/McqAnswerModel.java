package com.example.myquiz.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class McqAnswerModel implements Parcelable {
    @SerializedName("mc_id")
    @Expose
    private Integer mcId;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("mc_is_true_answer")
    @Expose
    private String mcIsTrueAnswer;
    @SerializedName("isUSer_select")
    @Expose
    private boolean isUser_select;
    @SerializedName("isSystemSelect")
    @Expose
    private boolean isSystem_select;

    protected McqAnswerModel(Parcel in) {
        if (in.readByte() == 0) {
            mcId = null;
        } else {
            mcId = in.readInt();
        }
        answer = in.readString();
        mcIsTrueAnswer = in.readString();
        isUser_select = in.readByte() != 0;
        isSystem_select = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (mcId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(mcId);
        }
        dest.writeString(answer);
        dest.writeString(mcIsTrueAnswer);
        dest.writeByte((byte) (isUser_select ? 1 : 0));
        dest.writeByte((byte) (isSystem_select ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<McqAnswerModel> CREATOR = new Creator<McqAnswerModel>() {
        @Override
        public McqAnswerModel createFromParcel(Parcel in) {
            return new McqAnswerModel(in);
        }

        @Override
        public McqAnswerModel[] newArray(int size) {
            return new McqAnswerModel[size];
        }
    };

    public boolean isUser_select() {
        return isUser_select;
    }

    public void setUser_select(boolean user_select) {
        isUser_select = user_select;
    }

    public boolean isSystem_select() {
        return isSystem_select;
    }

    public void setSystem_select(boolean system_select) {
        isSystem_select = system_select;
    }

    public Integer getMcId() {
        return mcId;
    }

    public void setMcId(Integer mcId) {
        this.mcId = mcId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getMcIsTrueAnswer() {
        return mcIsTrueAnswer;
    }

    public void setMcIsTrueAnswer(String mcIsTrueAnswer) {
        this.mcIsTrueAnswer = mcIsTrueAnswer;
    }


}
