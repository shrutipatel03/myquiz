package com.example.myquiz.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuestionDataModel implements Parcelable {
    @SerializedName("mq_id")
    @Expose
    private String mqId;
    @SerializedName("mq_question")
    @Expose
    private String mqQuestion;
    @SerializedName("mq_time")
    @Expose
    private String mqTime;
    @SerializedName("mq_level")
    @Expose
    private String mqLevel;
    @SerializedName("mcq_answer_master")
    @Expose
    private List<McqAnswerModel> mcqAnswerModels = null;
    @SerializedName("is_user_select_right")
    @Expose
    private boolean is_user_select_right;


    protected QuestionDataModel(Parcel in) {
        mqId = in.readString();
        mqQuestion = in.readString();
        mqTime = in.readString();
        mqLevel = in.readString();
        mcqAnswerModels = in.createTypedArrayList(McqAnswerModel.CREATOR);
        is_user_select_right = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mqId);
        dest.writeString(mqQuestion);
        dest.writeString(mqTime);
        dest.writeString(mqLevel);
        dest.writeTypedList(mcqAnswerModels);
        dest.writeByte((byte) (is_user_select_right ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuestionDataModel> CREATOR = new Creator<QuestionDataModel>() {
        @Override
        public QuestionDataModel createFromParcel(Parcel in) {
            return new QuestionDataModel(in);
        }

        @Override
        public QuestionDataModel[] newArray(int size) {
            return new QuestionDataModel[size];
        }
    };

    public boolean isIs_user_select_right() {
        return is_user_select_right;
    }

    public void setIs_user_select_right(boolean is_user_select_right) {
        this.is_user_select_right = is_user_select_right;
    }

    public List<McqAnswerModel> getMcqAnswerModels() {
        return mcqAnswerModels;
    }

    public void setMcqAnswerModels(List<McqAnswerModel> mcqAnswerModels) {
        this.mcqAnswerModels = mcqAnswerModels;
    }


    public String getMqId() {
        return mqId;
    }

    public void setMqId(String mqId) {
        this.mqId = mqId;
    }

    public String getMqQuestion() {
        return mqQuestion;
    }

    public void setMqQuestion(String mqQuestion) {
        this.mqQuestion = mqQuestion;
    }

    public String getMqTime() {
        return mqTime;
    }

    public void setMqTime(String mqTime) {
        this.mqTime = mqTime;
    }

    public String getMqLevel() {
        return mqLevel;
    }

    public void setMqLevel(String mqLevel) {
        this.mqLevel = mqLevel;
    }

    public List<McqAnswerModel> getMcqAnswerModel() {
        return mcqAnswerModels;
    }

    public void setMcqAnswerModel(List<McqAnswerModel> mcqAnswerMaster) {
        this.mcqAnswerModels = mcqAnswerMaster;
    }


}
