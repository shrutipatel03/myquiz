package com.example.myquiz.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class QuestionContainerModel implements Parcelable {

    List<QuestionDataModel> questionDataModelList;

    public QuestionContainerModel() {

    }

    protected QuestionContainerModel(Parcel in) {
        questionDataModelList = in.createTypedArrayList(QuestionDataModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(questionDataModelList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuestionContainerModel> CREATOR = new Creator<QuestionContainerModel>() {
        @Override
        public QuestionContainerModel createFromParcel(Parcel in) {
            return new QuestionContainerModel(in);
        }

        @Override
        public QuestionContainerModel[] newArray(int size) {
            return new QuestionContainerModel[size];
        }
    };

    public List<QuestionDataModel> getQuestionDataModelList() {
        return questionDataModelList;
    }

    public void setQuestionDataModelList(List<QuestionDataModel> questionDataModelList) {
        this.questionDataModelList = questionDataModelList;
    }
}
