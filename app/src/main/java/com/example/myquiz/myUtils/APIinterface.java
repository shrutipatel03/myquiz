package com.example.myquiz.myUtils;

import com.example.myquiz.model.QuestionModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface APIinterface {

    @FormUrlEncoded
    @POST("quiz/mcq_list")
    Call<QuestionModel> getQuestion(@Header("Authorization") String auth, @Field("category_id") String category_id, @Field("ph_id") String ph_id, @Field("type") String type);
}
