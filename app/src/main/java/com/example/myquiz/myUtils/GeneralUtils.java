package com.example.myquiz.myUtils;

import android.util.Base64;

import java.io.UnsupportedEncodingException;



public class GeneralUtils {

    public static String getAuthToken(){
        byte[] data = new byte[0];
        try {
            // MySharedPreference mySharedPreferences=new MySharedPreference(context);
            data = ("admin" + ":" + "1234").getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + android.util.Base64.encodeToString(data, Base64.NO_WRAP);
    }
}
